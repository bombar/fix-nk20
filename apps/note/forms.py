# Copyright (C) 2018-2020 by BDE ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

from dal import autocomplete
from django import forms
from django.utils.translation import gettext_lazy as _

from .models import Alias
from .models import Transaction, TransactionTemplate


class AliasForm(forms.ModelForm):
    class Meta:
        model = Alias
        fields = ("name",)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["name"].label = False
        self.fields["name"].widget.attrs = {"placeholder": _('New Alias')}


class ImageForm(forms.Form):
    image = forms.ImageField(required=False,
                             label=_('select an image'),
                             help_text=_('Maximal size: 2MB'))
    x = forms.FloatField(widget=forms.HiddenInput())
    y = forms.FloatField(widget=forms.HiddenInput())
    width = forms.FloatField(widget=forms.HiddenInput())
    height = forms.FloatField(widget=forms.HiddenInput())


class TransactionTemplateForm(forms.ModelForm):
    class Meta:
        model = TransactionTemplate
        fields = '__all__'

        # Le champ de destination est remplacé par un champ d'auto-complétion.
        # Quand des lettres sont tapées, une requête est envoyée sur l'API d'auto-complétion
        # et récupère les aliases valides
        # Pour force le type d'une note, il faut rajouter le paramètre :
        # forward=(forward.Const('TYPE', 'note_type') où TYPE est dans {user, club, special}
        widgets = {
            'destination':
                autocomplete.ModelSelect2(
                    url='note:note_autocomplete',
                    attrs={
                        'data-placeholder': 'Note ...',
                        'data-minimum-input-length': 1,
                    },
                ),
        }


class TransactionForm(forms.ModelForm):
    def save(self, commit=True):
        super().save(commit)

    def clean(self):
        """
        If the user has no right to transfer funds, then it will be the source of the transfer by default.
        Transactions between a note and the same note are not authorized.
        """

        cleaned_data = super().clean()
        if "source" not in cleaned_data:  # TODO Replace it with "if %user has no right to transfer funds"
            cleaned_data["source"] = self.user.note

        if cleaned_data["source"].pk == cleaned_data["destination"].pk:
            self.add_error("destination", _("Source and destination must be different."))

        return cleaned_data

    class Meta:
        model = Transaction
        fields = (
            'source',
            'destination',
            'reason',
            'amount',
        )

        # Voir ci-dessus
        widgets = {
            'source':
                autocomplete.ModelSelect2(
                    url='note:note_autocomplete',
                    attrs={
                        'data-placeholder': 'Note ...',
                        'data-minimum-input-length': 1,
                    },
                ),
            'destination':
                autocomplete.ModelSelect2(
                    url='note:note_autocomplete',
                    attrs={
                        'data-placeholder': 'Note ...',
                        'data-minimum-input-length': 1,
                    },
                ),
        }
